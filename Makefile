VERSION=v2
PREFIX_CMD= cd ./${VERSION}

build:
	$(PREFIX_CMD) && go build .

test:
	$(PREFIX_CMD) && go test -v -coverprofile=coverage.out  -timeout 30s -run .

coverage:
	$(PREFIX_CMD) && go tool cover -html=coverage.out

ci_coverage:
	$(PREFIX_CMD) && go tool cover -func=coverage.out
	
ci_coverage_check:
	 make ci_coverage | grep "total:" |grep "100.0%"

pre_review: start test coverage ci_coverage_check
