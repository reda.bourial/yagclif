module gitlab.com/reda.bourial/yagclif/v2

go 1.14

require (
	github.com/stretchr/testify v1.6.1
	gitlab.com/reda.bourial/catch v1.0.2
)
