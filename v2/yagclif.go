package yagclif

import (
	"bytes"
	"fmt"
	"os"
)

// concatenates the string array by adding a return to line
// at the end and a string at the beginning of each line.
func prependToArray(strs []string, prepend string) string {
	var buffer bytes.Buffer
	for _, str := range strs {
		buffer.WriteString(prepend)
		buffer.WriteString(str)
		buffer.WriteString("\r\n")
	}
	return buffer.String()
}

type runnable interface {
	GetHelp() []string
	RunWithArgs([]string) error
	Description() string
	HasUsage() bool
}

// App is an implementation of the cli app.
// It handles routing.
type App struct {
	name              string
	description       string
	routes            map[string]runnable
	outputHelpOnError bool
}

func (app *App) Description() string {
	return fmt.Sprintf("(subapp) %s", app.description)
}

func (app *App) isNameAvailable(name string) error {
	if app.routes[name] != nil {
		return fmt.Errorf(
			"route %s already used",
			name,
		)
	}
	return nil
}

// AddRoute is the methode for adding routes to the cli app.
func (app *App) AddApp(subApp *App) error {
	availabilityErr := app.isNameAvailable(subApp.name)
	if availabilityErr != nil {
		return availabilityErr
	}
	app.routes[subApp.name] = subApp
	return nil
}

// AddRoute is the methode for adding routes to the cli app.
func (app *App) AddRoute(route Route) error {
	availabilityErr := app.isNameAvailable(route.Name)
	if availabilityErr != nil {
		return availabilityErr
	}
	r, err := makeRoute(route)
	if err == nil {
		app.routes[route.Name] = r
	}
	return err
}

type Routes []Route

// AddRoutes is the methode for adding routes in bulk to the cli app.
func (app *App) AddRoutes(routes Routes) (errorIdx int, err error) {
	for idx, route := range routes {
		err = app.AddRoute(route)
		if err != nil {
			return idx, err
		}
	}
	return 0, err
}

// Run is the method to start running the cli app.
func (app *App) RunWithArgs(args []string) error {
	// formatError formats the error to output it.
	formatError := func(err interface{}) error {
		if app.outputHelpOnError {
			help := app.HelpString()
			return fmt.Errorf("%s\r\n%s\r\n", err, help)
		}
		return fmt.Errorf("%s", err)
	}
	if len(args) < 1 {
		err := formatError("no action was selected")
		return err
	}
	routeName := args[0]
	route := app.routes[routeName]
	if route == nil {
		errMsg := fmt.Sprintf("%s action not found", routeName)
		err := formatError(errMsg)
		return err
	}
	err := route.RunWithArgs(args[1:])
	if err != nil {
		errMsg := formatError(err)
		return errMsg
	}
	return nil
}

// getHelp returns an array string.
// Each element is a line of the help text.
func (app *App) GetHelp() []string {
	return []string{}
}

// HasUsage returns false to avoid extra long help text.
func (app *App) HasUsage() bool {
	return false
}

// HelpString return the help for the current cli app.
func (app *App) HelpString() string {
	var buffer bytes.Buffer
	writeln := func(s string) {
		buffer.WriteString(s)
		buffer.WriteString("\r\n")
	}
	writeln(app.name)
	writeln(app.description)
	writeln("")
	for routeName, route := range app.routes {
		routeTitle := fmt.Sprintf("\t %s : %s", routeName, route.Description())
		writeln(routeTitle)
		if route.HasUsage() {
			writeln("\t\t usage :")
		}
		routeArgsHelp := route.GetHelp()
		routeHelp := prependToArray(routeArgsHelp, "\t\t\t")
		writeln(routeHelp)
	}
	return buffer.String()
}

// Run is the method to start running the cli app.
func (app *App) Run() error {
	return app.RunWithArgs(os.Args[1:])
}

// NewCliApp creates a new cli app.
func NewCliApp(name string, description string, outputHelpOnError bool) *App {
	return &App{
		name:              name,
		description:       description,
		routes:            map[string]runnable{},
		outputHelpOnError: outputHelpOnError,
	}
}
