package yagclif

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrependToArray(t *testing.T) {
	s := prependToArray([]string{"hello", "world"}, "|")
	assert.Equal(t, "|hello\r\n|world\r\n", s)
}
func TestAddRoute(t *testing.T) {
	app := NewCliApp("Hello", "simple hello worlds", false)
	r := Route{
		Name:     "echo",
		Callback: func(args []string) {},
	}
	err := app.AddRoute(r)
	assert.Nil(t, err)
	assert.NotNil(t, app.routes["echo"])
	err = app.AddRoute(r)
	assert.NotNil(t, err)
}

func TestAddRoutes(t *testing.T) {
	routes := []Route{
		{
			Name:     "echo1",
			Callback: func(args []string) {},
		},
		{
			Name:     "echo2",
			Callback: func(args []string) {},
		},
	}
	t.Run("works", func(t *testing.T) {
		app := NewCliApp("Hello", "simple hello worlds", false)
		idx, err := app.AddRoutes(routes)
		assert.Equal(t, idx, 0)
		assert.Nil(t, err)
		assert.NotNil(t, app.routes["echo1"])
		assert.NotNil(t, app.routes["echo2"])
	})
	t.Run("errors", func(t *testing.T) {
		app := NewCliApp("Hello", "simple hello worlds", false)
		testRoutes := routes
		testRoutes[1].Name = "echo1"
		idx, err := app.AddRoutes(testRoutes)
		assert.NotNil(t, err)
		assert.Equal(t, idx, 1)
	})
}

func TestRun(t *testing.T) {
	type EmbededStruct struct {
		Embeded int `yagclif:"default:42"`
	}
	type TestStruct struct {
		EmbededStruct
		A []int `yagclif:"delimiter:,;mandatory"`
	}
	app := NewCliApp("Hello", "simple hello worlds", false)
	var passedStruct *TestStruct
	r := Route{
		Name:        "echo",
		Description: "echoes the args",
		Callback: func(testStruct TestStruct, args []string) {
			passedStruct = &testStruct
		},
	}
	err := app.AddRoute(r)
	assert.Nil(t, err)
	t.Run("works", func(t *testing.T) {
		os.Args = []string{"./main", "echo", "--a", "42,43", "world"}
		err = app.Run()
		assert.Nil(t, err)
		assert.NotNil(t, passedStruct)
		assert.Equal(t, TestStruct{
			EmbededStruct: EmbededStruct{Embeded: 42},
			A:             []int{42, 43},
		}, *passedStruct)
	})
	t.Run("missing route", func(t *testing.T) {
		os.Args = []string{"./main", "missingAction", "--a", "42,43", "world"}
		err = app.Run()
		assert.NotNil(t, err)
	})
	t.Run("no args", func(t *testing.T) {
		os.Args = []string{"./main"}
		err = app.Run()
		assert.NotNil(t, err)
	})
	t.Run("runtime error", func(t *testing.T) {
		os.Args = []string{"./main", "panic"}
		r := Route{
			Name:        "panic",
			Description: "just panic",
			Callback: func(args []string) {
				panic("u..rge...to panic .... can't help it !")
			},
		}
		err := app.AddRoute(r)
		assert.Nil(t, err)
		err = app.Run()
		assert.NotNil(t, err)
	})
}

func TestHelpString(t *testing.T) {
	type Context struct {
		AT int    `yagclif:"shortname:a;description:imA;mandatory"`
		BT string `yagclif:"description:FOO;default:someDefaultValue;env:testgethelp"`
	}
	os.Setenv("testgethelp", "something")
	fmt.Println("------------SAMPLE APP HELP------------")
	app := NewCliApp("Hello", "simple hello worlds", true)
	r := Route{
		Name:        "echo",
		Description: "echoes the args",
		Callback:    func([]string) {},
	}
	err := app.AddRoute(r)
	assert.Nil(t, err)
	r = Route{
		Name:        "someAction",
		Description: "does stuff",
		Callback:    func(Context, []string) {},
	}
	r = Route{
		Name:        "someAction",
		Description: "does stuff",
		Callback:    func(Context, []string) {},
	}
	err = app.AddRoute(r)
	assert.Nil(t, err)
	err = app.AddApp(app)
	assert.Nil(t, err)
	err = app.AddApp(app)
	assert.NotNil(t, err)
	help := app.HelpString()
	fmt.Println(help)
	assert.Contains(t, help, "Hello")
	assert.Contains(t, help, "simple hello worlds")
	assert.Contains(t, help, "someAction : does stuff")
	assert.Contains(t, help, "--at -a int (mandatory): imA")
	assert.Contains(t, help, "--bt string (default=someDefaultValue;env=testgethelp): FOO")

}

func TestRun2(t *testing.T) {
	type Context struct {
		AT int    `yagclif:"shortname:a;description:imA;mandatory"`
		BT string `yagclif:"description:FOO;default:someDefaultValue;env:testgethelp"`
	}
	os.Setenv("testgethelp", "something")
	fmt.Println("------------SAMPLE APP HELP------------")
	app := NewCliApp("Hello", "simple hello worlds", true)
	r := Route{
		Name: "echo",
		Callback: func([]string) {
			fmt.Println("here")
			panic("something")
		},
	}
	err := app.AddRoute(r)
	assert.Nil(t, err)
	err = app.RunWithArgs([]string{})
	assert.NotNil(t, err)
	os.Args = []string{"main.go", "echo"}
	err = app.Run()
	assert.NotNil(t, err)
	fmt.Println(err)
}
